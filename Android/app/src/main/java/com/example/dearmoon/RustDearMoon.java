package com.example.dearmoon;

public class RustDearMoon {

    // bridge
    // JNI constructs the name of the function that it will call is
    // Java_<domain>_<class>_<methodname>
    private static native String dearMoon(final String pattern);
    // Java_com_example_dearmoon_RustDearMoon_dearMoon

    public String talktomoon(String to) {
        return dearMoon(to);
    }
}
