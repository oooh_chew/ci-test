package com.example.dearmoon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("dearMoon");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RustDearMoon m = new RustDearMoon();
        String r = m.talktomoon("Android calling moon🖤");

        ((TextView)findViewById(R.id.moonField)).setText(r);
    }
}